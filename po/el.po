# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the missioncenter package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: missioncenter\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-08-18 19:41+0300\n"
"PO-Revision-Date: 2023-08-08 15:53+0000\n"
"Last-Translator: yiannis ioannides <sub@wai.ai>\n"
"Language-Team: Greek <https://hosted.weblate.org/projects/mission-center/"
"mission-center/el/>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.0-dev\n"

#: data/io.missioncenter.MissionCenter.desktop.in:3
#: data/io.missioncenter.MissionCenter.metainfo.xml.in:4
msgid "Mission Center"
msgstr "Κέντρο Ελέγχου"

#: data/io.missioncenter.MissionCenter.desktop.in:10
msgid ""
"Task manager;Resource monitor;System monitor;Processor;Processes;Performance "
"monitor;CPU;GPU;Disc;Disk;Memory;Network;Utilisation;Utilization"
msgstr ""
"Διαχείριση εφαρμογών;Επίβλεψη πόρων;Επίβλεψη συστήματος;Processor;Εφαρμογές;"
"Επίβλεψη επίδοσης;Επεξεργαστής;Κάρτα γραφικών;Δίσκος;Δίσκος;Μνήμη;Δίκτυο;"
"Χρήση;Χρήση"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:5
msgid "Mission Center Developers"
msgstr "Κέντρο Ελέγχου - Προγραμματιστές"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:14
msgid "Monitor system resource usage"
msgstr "Επίβλεψη χρήσης πόρων συστήματος"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:16
msgid "Monitor your CPU, Memory, Disk, Network and GPU usage"
msgstr ""
"Επιβλέψτε τη χρήση του επεξεργαστή, μνήμης, δίσκου, δικτύου και κάρτας "
"γραφικών του υπολογιστή σας"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:17
msgid "Features:"
msgstr "Χαρακτηριστικά:"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:19
msgid "Monitor overall or per-thread CPU usage"
msgstr "Επίβλεψη χρήσης του επεξεργαστή, ολικής ή ανά thread"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:20
msgid ""
"See system process, thread, and handle count, uptime, clock speed (base and "
"current), cache sizes"
msgstr ""
"Δείτε τις διεργασίες συστήματος, threads, χρόνο λειτουργίας, clock speed "
"(βασικό ή τρέχων), μέγεθος cache"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:21
msgid "Monitor RAM and Swap usage"
msgstr "Επίβλεψη χρήσης μνήμης και swap"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:22
msgid "See a breakdown how the memory is being used by the system"
msgstr "Δείτε αναλυτικά το τρόπο χρήσης της μνήμης από το σύστημα"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:23
msgid "Monitor Disk utilization and transfer rates"
msgstr "Επίβλεψη χρήσης δίσκου και ταχύτητα ανταλλαγής δεδομένων"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:24
msgid "Monitor network utilization and transfer speeds"
msgstr "Επίβλεψη χρήση δικτύου και ταχύτητα ανταλλαγής δεδομένων"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:25
msgid ""
"See network interface information such as network card name, connection type "
"(Wi-Fi or Ethernet), wireless speeds and frequency, hardware address, IP "
"address"
msgstr ""
"Δείτε τις πληροφορίες δικτύου, όπως το όνομα της κάρτας δικτύου, τύπου "
"σύνδεσης (Wi-Fi ή Ethernet), ταχύτητες wireless και συχνότητα, διεύθυνση "
"συσκευής και IP"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:26
msgid ""
"Monitor overall GPU usage, video encoder and decoder usage, memory usage and "
"power consumption, powered by the popular NVTOP project"
msgstr ""
"Επίβλεψη ολικής χρήσης κάρτας γραφικών, χρήσης κωδικοποίησης / "
"αποκωδικοποίησης βίντεο, κατανάλωσης μνήμης και ρεύματος, δια της γνωστής "
"εφαρμογής NVTOP"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:27
msgid "See a breakdown of resource usage by app and process"
msgstr "Δείτε αναλυτικά τη χρήση πόρων ανά εφαρμογή ή διεργασιών"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:28
msgid "Supports a minified summary view for simple monitoring"
msgstr "Υποστήριξη συνοπτικής εικόνας για απλή επίβλεψη"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:29
msgid ""
"Use OpenGL rendering for all the graphs in an effort to reduce CPU and "
"overall resource usage"
msgstr ""
"Χρήση OpenGL rendering για όλα τα γραφήματα με σκοπό την λιγότερη κατανάλωση "
"πόρων του επεξεργαστή"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:30
msgid "Uses GTK4 and Libadwaita"
msgstr "Χρησιμοποιεί GTK4 και Libadwaita"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:31
msgid "Written in Rust"
msgstr "Γραμμένο σε Rust"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:32
msgid "Flatpak first"
msgstr "Πρώτα Flatpak"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:34
msgid "Limitations (there is ongoing work to overcome all of these):"
msgstr "Περιορισμοί (προς επίλυση):"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:36
msgid ""
"The application currently only supports monitoring, you cannot stop "
"processes for example"
msgstr ""
"Προς το παρόν, η εφαρμογή υποστηρίζει μόνο επίβλεψη, δεν μπορεί πχ. να "
"διακόψει εφαρμογές"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:37
msgid "Disk utilization percentage might not be accurate"
msgstr "Το ποσοστό χρήσης δίσκου μπορεί να μην είναι ακριβές"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:38
msgid "No per-process network usage"
msgstr "Χωρίς ένδειξη χρήσης δικτύου ανά διεργασία"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:39
msgid "No per-process GPU usage"
msgstr "Χωρίς ένδειξη χρήσης κάρτας δικτύου ανά διεργασία"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:40
msgid ""
"GPU support is experimental and only AMD and nVidia GPUs can be monitored"
msgstr ""
"Η υποστήριξη επίβλεψης κάρτας γραφικών βρίσκεται ακόμη σε πειραματικό "
"στάδιο, μόνο για κάρτες AMD κια NVIDIA"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:42
msgid "Comments, suggestions, bug reports and contributions welcome"
msgstr "Σχόλια, προτάσεις, αναφορά σφαλμάτων και συνεισφορές είναι ευπρόσδεκτα"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:103
msgid "Add Spanish translation by Óscar Fernández Díaz"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:104
msgid ""
"The memory tab now shows configured memory speed instead of the maximum "
"supported by the modules"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:105
#, fuzzy
msgid "Add German translation by TecCheck"
msgstr "Προσθήκη μετάφρασης Τσέχικων από ondra05"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:106
msgid ""
"When a process uses large amounts of CPU or RAM it is now highlighted in the "
"Apps and Processes list"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:107
msgid "Add initial support for building for ARM64"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:108
msgid ""
"Take into account multiple CPU cores and cache sharing when calculating "
"cache sizes"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:109
msgid ""
"Fix browsers, installed as native packages, not showing up in the Apps list"
msgstr ""

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:110
#, fuzzy
msgid "Translation updates for Traditional Chinese by Peter Dave Hello"
msgstr "Προσθήκη μετάφρασης παραδοσιακών Κινέζικων από Peter Dave Hello"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:118
msgid "Translation fixes for Portuguese by Rafael Fontenelle"
msgstr "Διόρθωση μετάφρασης Πορτογαλικών από Rafael Fontenelle"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:119
msgid ""
"Only show a link-local IPv6 address if no other IPv6 exists by Maximilian"
msgstr ""
"Ένδειξη τοπικής διεύθυνσης IPv6 σε περίπτωση που δεν υπάρχει άλλη IPv6, από "
"Maximillian"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:120
msgid "Add Traditional Chinese locale by Peter Dave Hello"
msgstr "Προσθήκη μετάφρασης παραδοσιακών Κινέζικων από Peter Dave Hello"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:121
msgid "Add category for application menu by Renner0E"
msgstr "Προσθήκη κατηγορίας μενού εφαρμογών από Renner0E"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:122
msgid ""
"Fix a parsing error when parsing the output of `dmidecode` that lead to a "
"panic"
msgstr "Διόρθωση σφάλματος εξόδου μηνύματος `dmidecode`που οδηγούσε σε σφάλμα"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:123
msgid ""
"Use a fallback if `/sys/devices/system/cpu/cpu0/cpufreq/base_frequency` does "
"not exist, when getting CPU base speed information"
msgstr ""
"Χρήση εναλλακτικής σε περίπτωση που το μονοπάτι `/sys/devices/system/cpu/"
"cpu0/cpufreq/base_frequency`δεν υπάρχει, κατά τη διάρκεια ένδειξης ταχύτητας "
"του επεξεργαστή"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:124
msgid "Update GPU tab UI to be more adaptive for smaller resolutions"
msgstr ""
"Αναβάθμιση του πάνελ ένδειξης της κάρτας γραφικών, προσαρμόσιμο σε οθόνες "
"μικρότερης ευκρίνειας"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:132
msgid "Added Czech translation by ondra05"
msgstr "Προσθήκη μετάφρασης Τσέχικων από ondra05"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:133
msgid "Added Portuguese translation by Rilson Joás"
msgstr "Προσθήκη μετάφρασης Πορτογαλικών από Rilson Joás"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:134
msgid ""
"Add keywords to desktop file to improve search function of desktop "
"environments by Hannes Kuchelmeister"
msgstr ""
"Προσθήκη λέξεων-κλειδιών στο αρχείο desktop για την βελτίωση αναζήτησης σε "
"διάφορα περιβάλλοντα, από Hannes Kuchelmeister"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:135
msgid "Fixed a bug where the app and process list was empty for some users"
msgstr ""
"Διόρθωση σφάλματος οπού η λίστα εφαρμογών και διεργασιών ήταν άδεια για "
"κάποιους χρήστες"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:143
msgid "Fix a crash that occurs when the system is under heavy load"
msgstr "Διόρθωση κρασαρίσματος κατόπιν βαριάς χρήσης του συστήματος"

#: data/io.missioncenter.MissionCenter.metainfo.xml.in:151
msgid "First official release!"
msgstr "Πρώτη επίσημη κυκλοφορία!"

#: data/io.missioncenter.MissionCenter.gschema.xml:29
msgid "Which page is shown on application startup"
msgstr "Ποιά σελίδα θα φαίνεται κατά την εκκίνηση της εφαρμογής"

#: data/io.missioncenter.MissionCenter.gschema.xml:35
msgid "How fast should the data be refreshed and the UI updated"
msgstr "Πόσο γρήγορα θα ανανεώνεται η ροή των πληροφοριών"

#: data/io.missioncenter.MissionCenter.gschema.xml:40
#: resources/ui/preferences/page.blp:38
msgid "Parent and child process stats are shown individually or merged upwards"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:45
msgid "Column sorting is persisted across app restarts"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:50
msgid "The column id by which the Apps page view is sorted"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:55
msgid "The sorting direction of the Apps page view"
msgstr ""

#: data/io.missioncenter.MissionCenter.gschema.xml:61
msgid "Which graph is shown on the CPU performance page"
msgstr "Ποιό γράφημα θα φαίνεται στη σελίδα επίδοσης του επεξεργαστή"

#: data/io.missioncenter.MissionCenter.gschema.xml:66
msgid "Which page is shown on application startup, in the performance tab"
msgstr ""
"Ποιά σελίδα θα φαίνεται κατά την εκκίνηση της εφαρμογής, στη καρτέλα επίδοσης"

#: resources/ui/performance_page/cpu.blp:42 src/apps_page/mod.rs:1004
#: src/performance_page/mod.rs:267
msgid "CPU"
msgstr "Επεξεργαστής"

#: resources/ui/performance_page/cpu.blp:74
msgid "% Utilization"
msgstr "% χρήσης"

#: resources/ui/performance_page/cpu.blp:84
msgid "100%"
msgstr "100%"

#: resources/ui/performance_page/cpu.blp:152
#: resources/ui/performance_page/gpu.blp:227
msgid "Utilization"
msgstr "Χρήση"

#: resources/ui/performance_page/cpu.blp:174
msgid "Speed"
msgstr "Ταχύτητα"

#: resources/ui/performance_page/cpu.blp:201 src/apps_page/mod.rs:407
msgid "Processes"
msgstr "Διεργασίες"

#: resources/ui/performance_page/cpu.blp:223
msgid "Threads"
msgstr "Threads"

#: resources/ui/performance_page/cpu.blp:245
msgid "Handles"
msgstr "Ταυτότητα"

#: resources/ui/performance_page/cpu.blp:268
msgid "Up time"
msgstr "Χρόνος λειτουργίας"

#: resources/ui/performance_page/cpu.blp:294
msgid "Base Speed:"
msgstr "Ταχύτητα βάσης:"

#: resources/ui/performance_page/cpu.blp:303
msgid "Sockets:"
msgstr "Sockets:"

#: resources/ui/performance_page/cpu.blp:312
msgid "Virtual processors:"
msgstr "Εικονικές διεργασίες:"

#: resources/ui/performance_page/cpu.blp:321
msgid "Virtualization:"
msgstr "Εικονικοποίηση:"

#: resources/ui/performance_page/cpu.blp:330
msgid "Virtual machine:"
msgstr "Εικονικός υπολογιστής:"

#: resources/ui/performance_page/cpu.blp:339
msgid "L1 cache:"
msgstr "L1 cache:"

#: resources/ui/performance_page/cpu.blp:348
msgid "L2 cache:"
msgstr "L2 cache:"

#: resources/ui/performance_page/cpu.blp:357
msgid "L3 cache:"
msgstr "L3 cache:"

#: resources/ui/performance_page/cpu.blp:442
msgid "Change G_raph To"
msgstr "Αλλαγή γ_ραφήματος σε"

#: resources/ui/performance_page/cpu.blp:445
msgid "Overall U_tilization"
msgstr "Ολική χ_ρήση"

#: resources/ui/performance_page/cpu.blp:450
msgid "Logical _Processors"
msgstr "Λογικές _διεργασίες"

#: resources/ui/performance_page/cpu.blp:458
#: resources/ui/performance_page/disk.blp:386
#: resources/ui/performance_page/gpu.blp:549
#: resources/ui/performance_page/memory.blp:402
#: resources/ui/performance_page/network.blp:369
msgid "Graph _Summary View"
msgstr "Γράφημα _σύνοψης"

#: resources/ui/performance_page/cpu.blp:463
#: resources/ui/performance_page/disk.blp:391
#: resources/ui/performance_page/gpu.blp:554
#: resources/ui/performance_page/memory.blp:407
#: resources/ui/performance_page/network.blp:374
msgid "_View"
msgstr "_Προβολή"

#: resources/ui/performance_page/cpu.blp:466
#: resources/ui/performance_page/disk.blp:394
#: resources/ui/performance_page/gpu.blp:557
#: resources/ui/performance_page/memory.blp:410
#: resources/ui/performance_page/network.blp:377
msgid "CP_U"
msgstr "_Επεξεργαστής"

#: resources/ui/performance_page/cpu.blp:471
#: resources/ui/performance_page/disk.blp:399
#: resources/ui/performance_page/gpu.blp:562
#: resources/ui/performance_page/memory.blp:415
#: resources/ui/performance_page/network.blp:382
msgid "_Memory"
msgstr "_Μνήμη"

#: resources/ui/performance_page/cpu.blp:476
#: resources/ui/performance_page/disk.blp:404
#: resources/ui/performance_page/gpu.blp:567
#: resources/ui/performance_page/memory.blp:420
#: resources/ui/performance_page/network.blp:387
msgid "_Disk"
msgstr "_Δίσκος"

#: resources/ui/performance_page/cpu.blp:481
#: resources/ui/performance_page/disk.blp:409
#: resources/ui/performance_page/gpu.blp:572
#: resources/ui/performance_page/memory.blp:425
#: resources/ui/performance_page/network.blp:392
msgid "_Network"
msgstr "_Δίκτυο"

#: resources/ui/performance_page/cpu.blp:486
#: resources/ui/performance_page/disk.blp:414
#: resources/ui/performance_page/gpu.blp:577
#: resources/ui/performance_page/memory.blp:430
#: resources/ui/performance_page/network.blp:397
msgid "_GPU"
msgstr "_Κάρτα γραφικών"

#: resources/ui/performance_page/cpu.blp:494
#: resources/ui/performance_page/disk.blp:422
#: resources/ui/performance_page/gpu.blp:585
#: resources/ui/performance_page/memory.blp:438
#: resources/ui/performance_page/network.blp:410
msgid "_Copy"
msgstr "_Αντιγραφή"

#: resources/ui/performance_page/disk.blp:64
#: resources/ui/performance_page/disk.blp:188
msgid "Active time"
msgstr "Ενεργός χρόνος"

#: resources/ui/performance_page/disk.blp:122
msgid "Disk transfer rate"
msgstr "Ταχύτητα ανταλλαγής δεδομένων δίσκου"

#: resources/ui/performance_page/disk.blp:211
msgid "Average response time"
msgstr "Μέσος χρόνος απάντησης"

#: resources/ui/performance_page/disk.blp:247
msgid "Read speed"
msgstr "Ταχύτητα read"

#: resources/ui/performance_page/disk.blp:279
msgid "Write speed"
msgstr "Ταχύτητα write"

#: resources/ui/performance_page/disk.blp:307
msgid "Capacity:"
msgstr "Χωρητικότητα:"

#: resources/ui/performance_page/disk.blp:316
msgid "Formatted:"
msgstr "Διαμορφώθηκε:"

#: resources/ui/performance_page/disk.blp:325
msgid "System disk:"
msgstr "Δίσκος συστήματος:"

#: resources/ui/performance_page/disk.blp:334
#: resources/ui/performance_page/memory.blp:345
msgid "Type:"
msgstr "Τύπος:"

#: resources/ui/performance_page/gpu.blp:71
msgid "Overall utilization"
msgstr "Ολική χρήση"

#: resources/ui/performance_page/gpu.blp:110
msgid "Video encode"
msgstr "Κωδικοποίηση βίντεο"

#: resources/ui/performance_page/gpu.blp:143
msgid "Video decode"
msgstr "Αποκωδικοποίηση βίντεο"

#: resources/ui/performance_page/gpu.blp:180
#: resources/ui/performance_page/gpu.blp:350
#: resources/ui/performance_page/memory.blp:73
msgid "Memory usage"
msgstr "Χρήση μνήμης"

#: resources/ui/performance_page/gpu.blp:250
msgid "Clock Speed"
msgstr "Clock speed"

#: resources/ui/performance_page/gpu.blp:295
msgid "Power draw"
msgstr "Κατανάλωση ενέργειας"

#: resources/ui/performance_page/gpu.blp:395
msgid "Memory speed"
msgstr "Ταχύτητα μνήμης"

#: resources/ui/performance_page/gpu.blp:440
msgid "Temperature"
msgstr "Θερμοκρασία"

#: resources/ui/performance_page/gpu.blp:470
msgid "OpenGL version:"
msgstr "Έκδοση OpenGL:"

#: resources/ui/performance_page/gpu.blp:479
msgid "Vulkan version:"
msgstr "Έκδοση Vulkan:"

#: resources/ui/performance_page/gpu.blp:488
msgid "PCI Express speed:"
msgstr "Ταχύτητα PCI Express:"

#: resources/ui/performance_page/gpu.blp:497
msgid "PCI bus address:"
msgstr "Διεύθυνση PCI bus:"

#: resources/ui/performance_page/memory.blp:28
msgid "Some information requires administrative privileges"
msgstr "Κάποιες πληροφορίες χρειάζονται προνόμια διαχειριστή"

#: resources/ui/performance_page/memory.blp:29
msgid "_Authenticate"
msgstr "_Επιβεβαίωση"

#: resources/ui/performance_page/memory.blp:52 src/apps_page/mod.rs:1010
#: src/performance_page/mod.rs:318
msgid "Memory"
msgstr "Μνήμη"

#: resources/ui/performance_page/memory.blp:130
msgid "Memory composition"
msgstr "Σύνθεση μνήμης"

#: resources/ui/performance_page/memory.blp:166
msgid "In use"
msgstr "Σε χρήση"

#: resources/ui/performance_page/memory.blp:189
msgid "Available"
msgstr "Διαθέσιμο"

#: resources/ui/performance_page/memory.blp:217
msgid "Committed"
msgstr "Μη διαθέσιμο"

#: resources/ui/performance_page/memory.blp:240
msgid "Cached"
msgstr "Σε cache"

#: resources/ui/performance_page/memory.blp:267
msgid "Swap available"
msgstr "Διαθέσιμη swap"

#: resources/ui/performance_page/memory.blp:290
msgid "Swap used"
msgstr "Swap σε χρήση"

#: resources/ui/performance_page/memory.blp:318
msgid "Speed:"
msgstr "Ταχύτητα:"

#: resources/ui/performance_page/memory.blp:327
msgid "Slots used:"
msgstr "Slots σε χρήση:"

#: resources/ui/performance_page/memory.blp:336
msgid "Form factor:"
msgstr "Form factor:"

#: resources/ui/performance_page/network.blp:62
msgid "Throughput"
msgstr "Ταχύτητα εισερχόμενων δεδομένων"

#: resources/ui/performance_page/network.blp:139
msgid "Send"
msgstr "Αποστολή"

#: resources/ui/performance_page/network.blp:170
msgid "Receive"
msgstr "Λήψη"

#: resources/ui/performance_page/network.blp:197
msgid "Interface name:"
msgstr "Όνομα συσκευής:"

#: resources/ui/performance_page/network.blp:206
msgid "Connection type:"
msgstr "Τύπος σύνδεσης:"

#: resources/ui/performance_page/network.blp:216
msgid "SSID:"
msgstr "SSID:"

#: resources/ui/performance_page/network.blp:226
msgid "Signal strength:"
msgstr "Δύναμη σήματος:"

#: resources/ui/performance_page/network.blp:236
msgid "Maximum Bitrate:"
msgstr "Μέγιστο bitrate:"

#: resources/ui/performance_page/network.blp:246
msgid "Frequency:"
msgstr "Συχνότητα:"

#: resources/ui/performance_page/network.blp:255
msgid "Hardware address:"
msgstr "Διεύθυνση hardware:"

#: resources/ui/performance_page/network.blp:264
msgid "IPv4 address:"
msgstr "Διεύθυνση IPv4:"

#: resources/ui/performance_page/network.blp:273
msgid "IPv6 address:"
msgstr "Διεύθυνση IPv6:"

#: resources/ui/performance_page/network.blp:403
msgid "Network Se_ttings"
msgstr "Ρ_υθμίσεις Δικτύου"

#: resources/ui/preferences/page.blp:6
msgid "General Settings"
msgstr "Γενικές ρυθμίσεις"

#: resources/ui/preferences/page.blp:9
msgid "Update Speed"
msgstr "Ταχύτητα ανανέωσης"

#: resources/ui/preferences/page.blp:12 src/preferences/page.rs:113
#: src/preferences/page.rs:262
msgid "Fast"
msgstr "Γρήγορη"

#: resources/ui/preferences/page.blp:13
msgid "Refresh every half second"
msgstr "Ανανέωση κάθε μισό δευτερόλεπτο"

#: resources/ui/preferences/page.blp:17 src/preferences/page.rs:109
#: src/preferences/page.rs:122 src/preferences/page.rs:266
#: src/preferences/page.rs:282
msgid "Normal"
msgstr "Κανονική"

#: resources/ui/preferences/page.blp:18
msgid "Refresh every second"
msgstr "Ανανέωση κάθε δευτερόλεπτο"

#: resources/ui/preferences/page.blp:22 src/preferences/page.rs:105
#: src/preferences/page.rs:270
msgid "Slow"
msgstr "Αργή"

#: resources/ui/preferences/page.blp:23
msgid "Refresh every second and a half"
msgstr "Ανανέωση κάθε ενάμιση δευτερόλεπτο"

#: resources/ui/preferences/page.blp:27 src/preferences/page.rs:101
#: src/preferences/page.rs:274
msgid "Very Slow"
msgstr "Πολύ αργή"

#: resources/ui/preferences/page.blp:28
msgid "Refresh every 2 seconds"
msgstr "Ανανέωση κάθε 2 δευτερόλεπτα"

#: resources/ui/preferences/page.blp:34
#, fuzzy
msgid "App Page Settings"
msgstr "Γενικές ρυθμίσεις"

#: resources/ui/preferences/page.blp:37
msgid "Merge Process Stats"
msgstr ""

#: resources/ui/preferences/page.blp:42
msgid "Remember Sorting"
msgstr ""

#: resources/ui/preferences/page.blp:43
#, fuzzy
msgid "Remember the sorting of the app and process list across app restarts"
msgstr ""
"Διόρθωση σφάλματος οπού η λίστα εφαρμογών και διεργασιών ήταν άδεια για "
"κάποιους χρήστες"

#: resources/ui/window.blp:48
msgid "Type a name or PID to search"
msgstr "Αναζήτηση μέσω ονόματος ή PID"

#: resources/ui/window.blp:90
msgid "Loading..."
msgstr ""

#: resources/ui/window.blp:104
msgid "Performance"
msgstr "Επίδοση"

#: resources/ui/window.blp:113 src/apps_page/mod.rs:400
msgid "Apps"
msgstr "Εφαρμογές"

#: resources/ui/window.blp:124
msgid "_Preferences"
msgstr "_Προτιμήσεις"

#: resources/ui/window.blp:129
msgid "_About MissionCenter"
msgstr "_Σχετικά με το Κέντρο Ελέγχου"

#: src/apps_page/mod.rs:992
msgid "Name"
msgstr "Όνομα"

#: src/apps_page/mod.rs:998
msgid "PID"
msgstr "PID"

#: src/apps_page/mod.rs:1016
msgid "Disk"
msgstr "Δίσκος"

#. ContentType::App
#: src/apps_page/list_item.rs:315
msgid "Stop Application"
msgstr ""

#: src/apps_page/list_item.rs:315
msgid "Force Stop Application"
msgstr ""

#. ContentType::Process
#: src/apps_page/list_item.rs:319
#, fuzzy
msgid "Stop Process"
msgstr "Διεργασίες"

#: src/apps_page/list_item.rs:319
msgid "Force Stop Process"
msgstr ""

#: src/performance_page/widgets/mem_composition_widget.rs:224
msgid ""
"In use ({}B)\n"
"\n"
"Memory used by the operating system and running applications"
msgstr ""
"Σε χρήση ({}B)\n"
"\n"
"Μνήμη που χρησιμοποιείται από το τρέχον λογισμικό και εφαρμογές"

#: src/performance_page/widgets/mem_composition_widget.rs:236
msgid ""
"Modified ({}B)\n"
"\n"
"Memory whose contents must be written to disk before it can be used by "
"another process"
msgstr ""
"Διαμορφώθηκε ({}B)\n"
"\n"
"Μνήμη που πρέπει να γραφτεί στο δίσκο προτού επαναχρησιμοποιηθεί από άλλη "
"διεργασία"

#: src/performance_page/widgets/mem_composition_widget.rs:257
msgid ""
"Standby ({}B)\n"
"\n"
"Memory that contains cached data and code that is not actively in use"
msgstr ""
"Σε αναμονή ({}B)\n"
"\n"
"Μνήμη που περιέχει ανενεργά δεδομένα cache"

#: src/performance_page/widgets/mem_composition_widget.rs:266
msgid ""
"Free ({}B)\n"
"\n"
"Memory that is not currently in use, and that will be repurposed first when "
"the operating system, drivers, or applications need more memory"
msgstr ""
"Ελεύθερη ({}B)\n"
"\n"
"Μνήμη που είναι ανενεργή και αμέσως διαθέσιμη όταν το λογισμικό, drivers και "
"εφαρμογές την χρειαστούν"

#: src/performance_page/mod.rs:371 src/performance_page/disk.rs:195
msgid "Disk {} ({})"
msgstr "Δίσκος {} ({})"

#: src/performance_page/mod.rs:375
msgid "HDD"
msgstr "HDD"

#: src/performance_page/mod.rs:376
msgid "SSD"
msgstr "SSD"

#: src/performance_page/mod.rs:377
msgid "NVMe"
msgstr "NVMe"

#: src/performance_page/mod.rs:378
msgid "eMMC"
msgstr "eMMC"

#: src/performance_page/mod.rs:379
msgid "iSCSI"
msgstr "iSCSI"

#: src/performance_page/mod.rs:380 src/performance_page/cpu.rs:278
#: src/performance_page/cpu.rs:291 src/performance_page/cpu.rs:301
#: src/performance_page/cpu.rs:307 src/performance_page/gpu.rs:255
#: src/performance_page/memory.rs:338 src/performance_page/network.rs:375
#: src/performance_page/network.rs:396 src/performance_page/network.rs:404
#: src/performance_page/network.rs:441 src/performance_page/network.rs:519
msgid "Unknown"
msgstr "Άγνωστο"

#: src/performance_page/mod.rs:450 src/performance_page/network.rs:331
msgid "Ethernet"
msgstr "Ethernet"

#: src/performance_page/mod.rs:451 src/performance_page/network.rs:338
msgid "Wi-Fi"
msgstr "Wi-Fi"

#: src/performance_page/mod.rs:452 src/performance_page/network.rs:340
msgid "Other"
msgstr "Άλλο"

#: src/performance_page/mod.rs:541
msgid "GPU {}"
msgstr "Κάρτα γραφικών {}"

#: src/performance_page/mod.rs:695
msgid "{}: {} {}bps {}: {} {}bps"
msgstr "{}: {} {}bps {}: {} {}bps"

#: src/performance_page/cpu.rs:286
msgid "Supported"
msgstr "Υποστηρίζεται"

#: src/performance_page/cpu.rs:288 src/performance_page/gpu.rs:266
msgid "Unsupported"
msgstr "Δεν υποστηρίζεται"

#: src/performance_page/cpu.rs:296 src/performance_page/disk.rs:223
msgid "Yes"
msgstr "Ναι"

#: src/performance_page/cpu.rs:298 src/performance_page/disk.rs:225
msgid "No"
msgstr "Όχι"

#: src/performance_page/cpu.rs:655
msgid "% Utilization over {} seconds"
msgstr "% χρήση σε {} δευτερόλεπτα"

#: src/performance_page/cpu.rs:659 src/performance_page/disk.rs:376
#: src/performance_page/memory.rs:401 src/performance_page/network.rs:605
msgid "{} seconds"
msgstr "{} δευτερόλεπτα"

#: src/performance_page/disk.rs:251
msgid "{} {}{}B/s"
msgstr "{} {}{}B/s"

#: src/performance_page/network.rs:415 src/performance_page/network.rs:421
#: src/performance_page/network.rs:429
msgid "{} {}bps"
msgstr "{} {}bps"

#: src/performance_page/network.rs:454 src/performance_page/network.rs:470
msgid "N/A"
msgstr "Μ/Δ"

#: src/application.rs:261
msgid "translator-credits"
msgstr "Yiannis Ioannides"

#~ msgid "_Quit"
#~ msgstr "_Κλείσιμο"
